# GitLab API for LabVIEW

Automate GitLab by using its simple and powerful REST API.

Interested in CI/CD with LabVIEW? Then visit our Release Automation Tools at https://rat.hampel-soft.com. Our tools help automate the testing, documenting, building, packaging and publishing of your projects. Built-in support for Git lets you trigger our tools from your repository, via GitLab CI/CD or Azure DevOps amongst others. 


## :bulb: Documentation

* https://docs.gitlab.com/ee/api/
* https://dokuwiki.hampel-soft.com/code/open-source/gitlab-api-for-labview


## :rocket: Installation

Download the latest VI Package at https://www.vipm.io/package/hse_lib_gitlab_api and install it globally with the [VI Package Manager](https://www.vipm.io/download/). 

### :wrench: LabVIEW 2016

The VIs are maintained in LabVIEW 2016.

### :link: Dependencies

Apply the `gitlab-api-for-labview.vipc` file to install the needed VI packages.


## :bulb: Usage

In the NI Example Finder Browse according to Directory Structure and navigate to \Hampel Software Engineering\GitLab API for examples. Insert your private token in <LabVIEW Data>\gitlabapi\config.ini in order to use the examples.


## :busts_in_silhouette: Contributing 

Please take a look at the [issue tracker](https://gitlab.com/hampel-soft/open-source/gitlab-api-for-labview/issues) for a list of potential tasks.

Our Dokuwiki holds more information on [how to collaborate](https://dokuwiki.hampel-soft.com/processes/collaboration) on an open-source project, in case you need help with the processes. Please [get in touch with us](office@hampel-soft.com) for any further questions.

##  :beers: Credits

The VIs in this repository are created and provided by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com). 

Contributors:
* Joerg Hampel
* Olivier Jourdan / Delacor
* Matthias Baudot / Delacor

## :page_facing_up: License 

This repository and its contents are licensed under a BSD/MIT like license - see the [LICENSE](LICENSE) file for details
